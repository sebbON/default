import InputParser from '../src/services/input-parser';

describe('ReadInput tests', () => {


  it.each([
    [
      `2
10 22
E 2
N 1`,
      { x: 10, y: 22 },
    ],[
      `2
-10 42
E 2
N 1`,
      { x: -10, y: 42 },
    ],
  ])('should read start vertex', (input, expected) => {
    const inputParser = new InputParser(input);
    expect(inputParser.startVertex).toEqual(expected);
  });

  it.each([
    [
      `2
10 22
E 2
N 1`,
      2,
    ],[
      `4
-10 42
E 2
N 1
S 3
W 16
S 13`,
      4,
    ],
  ])('should read x number of instructions', (input, expected) => {
    const inputParser = new InputParser(input);
    expect(inputParser.instructions.length).toEqual(expected);
  });

  it.each([
    [
      `2
10 22
E 2
N 1`,
[
  { direction: 'E', steps: 2 },
  { direction: 'N', steps: 1 },
],
    ],[
      `4
-10 42
E 2
N 1
S 3
W 16
S 13`,
[
  { direction: 'E', steps: 2 },
  { direction: 'N', steps: 1 },
  { direction: 'S', steps: 3 },
  { direction: 'W', steps: 16 },
],
    ],
  ])('should read correct instructions', (input, expected) => {
    const inputParser = new InputParser(input);
    expect(inputParser.instructions).toEqual(expected);
  });
});
