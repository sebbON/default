import Vacuum from './../src/services/vacuum';

describe('Vacuum tests', () => {
  it('should clean start vertex', () => {
    const vacuum = new Vacuum({ x: 1, y: 1 });

    expect(vacuum.execute([])).toEqual([{ x: 1, y: 1 }]);
  });

  it.each([
    [
      { x: 1, y: 1 },
      { direction: 'E', steps: 3 },
      [
        { x: 1, y: 1 },
        { x: 2, y: 1 },
        { x: 3, y: 1 },
        { x: 4, y: 1 },
      ],
    ],
    [
      { x: 1, y: 1 },
      { direction: 'W', steps: 3 },
      [
        { x: 1, y: 1 },
        { x: 0, y: 1 },
        { x: -1, y: 1 },
        { x: -2, y: 1 },
      ],
    ],
    [
      { x: 1, y: 1 },
      { direction: 'N', steps: 3 },
      [
        { x: 1, y: 1 },
        { x: 1, y: 2 },
        { x: 1, y: 3 },
        { x: 1, y: 4 },
      ],
    ],
    [
      { x: 1, y: 1 },
      { direction: 'S', steps: 3 },
      [
        { x: 1, y: 1 },
        { x: 1, y: 0 },
        { x: 1, y: -1 },
        { x: 1, y: -2 },
      ],
    ],
  ])(
    'should execute instruction in all directions',
    (startVertex, instruction, expected) => {
      const vacuum = new Vacuum(startVertex);
      expect(vacuum.execute([instruction])).toEqual(expected);
    }
  );

  it.each([
    [
      { x: 1, y: 1 },
      [
        { direction: 'E', steps: 3 },       //----
        { direction: 'W', steps: 3 },
      ],
      4,
    ],
    [
      { x: 1, y: 1 },
      [
        { direction: 'E', steps: 3 },       // --|  
        { direction: 'N', steps: 2 },       // | |
        { direction: 'W', steps: 2 },       //-+--
        { direction: 'S', steps: 3 }        // |
      ],
      10,
    ],
  ])(
    'should only count unique vertices',
    (startVertex, instructions, expectedNoCleanedVertices) => {
      const vacuum = new Vacuum(startVertex);
      expect(vacuum.execute(instructions).length).toEqual(
        expectedNoCleanedVertices
      );
    }
  );
});
