import InputParser from './services/input-parser'
import Vacuum from './services/vacuum'
import { inputString } from './input'

const runExample = (): string => {
  const inputParser = new InputParser(inputString);
  const vacuum = new Vacuum(inputParser.startVertex);
  return `=> Cleaned: ${vacuum.execute(inputParser.instructions).length}`
};

console.log(runExample());