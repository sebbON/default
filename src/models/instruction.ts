export default interface Instruction {
    direction: string;
    steps: number;
}