import Instruction from '../models/instruction';
import Vertex from '../models/vertex';

export default class InputParser {
  private _numberOfCommands: number;
  private _startVertex: Vertex;
  private _instructions: ReadonlyArray<Instruction>

  get startVertex(): Vertex {
    return this._startVertex;
  }

  get instructions(): ReadonlyArray<Instruction> {
    return this._instructions;
  }

  constructor(input: string) {
    this._numberOfCommands = this.readNumberofCommands(input);
    this._startVertex = this.readStartVertex(input);
    this._instructions = this.readInstructions(this._numberOfCommands, input);
  }
  
  private readNumberofCommands = (input: string): number =>
    parseInt(input.split('\n')[0]);

  private readStartVertex = (input: string): Vertex => {
    const vertexData = input
      .split('\n')[1]
      .split(' ');
    return { x: parseInt(vertexData[0]), y: parseInt(vertexData[1]) };
  };

  private readInstructions = (
    numberOfInstructions: number,
    input: string
  ): ReadonlyArray<Instruction> =>
    input
      .split('\n')
      .slice(2, 2 + numberOfInstructions)
      .map(i => {
        const unparsedInstruction = i.split(' ');
        return {
          direction: unparsedInstruction[0],
          steps: parseInt(unparsedInstruction[1]),
        };
      });
}
