import Instruction from '../models/instruction';
import Vertex from '../models/vertex';

export default class Vacuum {
  private cleanedVertices: ReadonlyArray<Vertex>;

  constructor(startVertex: Vertex) {
    this.cleanedVertices = [startVertex];
  }

  execute(instructions: ReadonlyArray<Instruction>): ReadonlyArray<Vertex> {
    instructions.forEach(instruction => this.executeInstruction(instruction));
    return this.getUniqueCleanedVertices();
  }

  private executeInstruction(instruction: Instruction): void {
    switch (instruction.direction) {
      case 'E':
        this.goVertical(instruction.steps);
        break;
      case 'W':
        this.goVertical(-instruction.steps);
        break;
      case 'N':
        this.goHorizontal(instruction.steps);
        break;
      case 'S':
        this.goHorizontal(-instruction.steps);
        break;
      default:
        break;
    }
  }

  private goHorizontal(steps: number) {
    for (let index = 0; index < Math.abs(steps); index++) {
      this.cleanedVertices = [
        ...this.cleanedVertices,
        {
          x: this.cleanedVertices[this.cleanedVertices.length - 1].x,
          y:
            this.cleanedVertices[this.cleanedVertices.length - 1].y +
            Math.sign(steps) * 1,
        },
      ];
    }
  }

  private goVertical(steps: number) {
    for (let index = 0; index < Math.abs(steps); index++) {
      this.cleanedVertices = [
        ...this.cleanedVertices,
        {
          x:
            this.cleanedVertices[this.cleanedVertices.length - 1].x +
            Math.sign(steps) * 1,
          y: this.cleanedVertices[this.cleanedVertices.length - 1].y,
        },
      ];
    }
  }

  private getUniqueCleanedVertices(): ReadonlyArray<Vertex> {
      return this.cleanedVertices.reduce((uniques: ReadonlyArray<Vertex>, vertex) => [...uniques, ...(uniques.every(u => u.x !== vertex.x || u.y !== vertex.y) ? [vertex] : [])], []);
  }
}
